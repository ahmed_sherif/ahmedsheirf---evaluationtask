@extends('layouts.app', ['body_class' => 'events-view'])
@section('title', 'Events')
@section('content')
    <script>
        fbq('track', 'PageView');
        function onEventClick() {
            fbq('track', 'ViewContent');
        }
    </script>
    <!-- Start Content -->
    <section class="hero-section d-flex justify-content-center align-items-center no-padding">
        <div class="hero-content d-flex justify-content-center align-items-center flex-column">
            <h1>Events</h1>
        </div>
        <img src="/images/events.jpg" alt="Hero Image" class="hero-image">
    </section>

    <section
        class="events-section container no-separator"
    >

        @if($events)
        <table class="table table-hover" >
            <thead class="thead-light">
            <tr>

                <th>city</th>
                <th>country</th>
                <th>name</th>
                <th>details</th>
                <th>address</th>
                <th>published</th>
                <th>coming_soon</th>
                <th>cancel_availability</th>
                <th>event_start</th>
                <th>event_end</th>


            </tr>
            </thead>
            <tbody>
            @foreach($events as $event)
                <tr>
                    <td>{{$event->city}}</td>
                    <td>{{$event->country}}</td>
                    <td>{{$event->name}}</td>
                    <td>{{$event->details}}</td>
                    <td>{{$event->address}}</td>
                    <td>{{$event->published}}</td>
                    <td>{{$event->coming_soon}}</td>
                    <td>{{$event->cancel_availablility}}</td>
                    <td>{{$event->event_start->diffForHumans()}}</td>
                    <td>{{$event->event_end->diffForHumans()}}</td>

                 </tr>
            </tbody>
                @endforeach
                </table>
        @endif
            {{$events->links()}}

    </section>

    <!-- End Content -->
@endsection
