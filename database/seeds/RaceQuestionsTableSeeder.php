<?php

use Illuminate\Database\Seeder;

class RaceQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 1,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 2,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 1,
            'question_id' => 1,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 1,
            'question_id' => 31,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 1,
            'question_id' => 3,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 1,
            'question_id' => 5,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 1,
            'question_id' => 4,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 1,
            'question_id' => 6,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 1,
            'question_id' => 7,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 1,
            'question_id' => 8,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 1,
            'question_id' => 2,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 1,
            'question_id' => 9,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 9,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 5,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 3,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 10,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 31,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 4,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 7,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 8,
        ]);

        // 1
        DB::table('race_question')->insert([
            'race_id' => 6,
            'question_id' => 6,
        ]);
       
    }
}
