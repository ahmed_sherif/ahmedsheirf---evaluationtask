<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Resources\EventResourceCollectionTask;
use Illuminate\Http\Request;

class EventTaskApiController extends Controller
{

    public function index() {
        $events = Event::paginate(2);
        return new EventResourceCollectionTask($events);
    }

}
