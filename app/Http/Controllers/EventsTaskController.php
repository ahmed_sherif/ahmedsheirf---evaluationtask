<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class EventsTaskController extends Controller
{

    public function index(){

        return view('event-task.index' , ['events' => Event::simplePaginate(2)]);
    }



}
